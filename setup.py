from setuptools import setup

from src import __version__


setup(
    name='mortgage_calc',
    version=__version__,
    scripts=['bin/mortgage_calc'],
    packages=['mortgage_calc'],
    package_dir={'mortgage_calc': 'src'},
    install_requires=[
        'Flask >= 0.12',
        'flasgger >= 0.5.14'
    ]
)

# Mortgage Calculator API

## Requirements

The API server can be run either through a local installation or
through Docker.

### Local Installation

* Python 2.7 (https://www.python.org/downloads/release/python-2712/)
* pip (https://pip.pypa.io/en/stable/reference/pip_install/)

### Docker

* Docker >= 1.10.0 (https://www.docker.com/)
* Docker Compose >= 1.11.2 (https://docs.docker.com/compose/)

## Quickstart

### Running Locally

1. Install via pip:

    ```
    $ pip install .
    ```

2. Run the server:

    ```
    $ mortgage_calc
     * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
    ```

3. Test the API via Swagger:

    ```
    http://127.0.0.1:5000/apidocs/index.html
    ```

### Running with Docker

1. Build the Docker image and run the server with Docker Compose:

    ```
    $ docker-compose up -d
    ````

2. Test the API via Swagger:

    ```
    http://[DOCKER_HOST]:5000/apidocs/index.html
    ```

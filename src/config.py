
PAYMENT_SCHEDULE_PERIODS = {
    'weekly': 52,
    'biweekly': 26,
    'monthly': 12
}

DEFAULT_INTEREST = 0.025

# Maps down payment percent ranges to insurance rates.
# Keys are (min_percent, non_inclusive_max_percent).
DOWN_PAYMENT_INSURANCE_MAP = {
    (0.05, 0.1): 0.0315,
    (0.1, 0.15): 0.024,
    (0.15, 0.2): 0.018
}

MAX_MORTGAGE_INSURANCE_THRESHOLD = 1000000

ALLOWED_AMORTIZATION_RANGE_YEARS = (5, 25)

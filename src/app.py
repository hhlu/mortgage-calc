import traceback

from flask import Flask, request, json
from flasgger import Swagger

from calculator import Calculator
from config import ALLOWED_AMORTIZATION_RANGE_YEARS, PAYMENT_SCHEDULE_PERIODS
from errors import ValidationError
from log import get_logger
from version import VERSION


logger = get_logger(__name__)


class CalculatorApp(object):
    def __init__(self):
        self.server = Flask(__name__)
        self.server.config['SWAGGER'] = {
            'specs': [
                {
                    'version': VERSION,
                    'title': 'Mortgage Calculator',
                    'description': 'A mortgage calculator API',
                    'endpoint': 'spec',
                    'route': '/spec'
                }
            ]
        }
        Swagger(self.server)

        self.server.add_url_rule('/payment-amount',
                                 'get_recurring_payment',
                                 self.get_recurring_payment,
                                 methods=['GET'])

        self.server.add_url_rule('/mortgage-amount',
                                 'get_maximum_mortgage',
                                 self.get_maximum_mortgage,
                                 methods=['GET'])

        self.server.add_url_rule('/interest-rate',
                                 'set_interest_rate',
                                 self.set_interest_rate,
                                 methods=['PATCH'])

        self.server.register_error_handler(
            Exception, self.global_error_handler)
        self.server.register_error_handler(
            ValidationError, self.validation_error_handler)

        self.calculator = Calculator()

    def run(self, host='0.0.0.0', port=5000):
        self.server.run(host, port)

    def global_error_handler(self, _):
        logger.error(traceback.format_exc())

        return self.server.response_class(
            response=json.dumps('An unexpected error occurred'),
            status=500,
            mimetype='application/json'
        )

    def validation_error_handler(self, e):
        error_message = e.message or 'Invalid input parameters'

        return self.server.response_class(
            response=json.dumps(error_message),
            status=400,
            mimetype='application/json'
        )

    def get_recurring_payment(self):
        """
        Get the recurring payment amount of a mortgage.
        ---
        tags:
            - calculation
        parameters:
            - name: asking_price
              in: query
              description: The asking price.
              required: true
              type: number
            - name: down_payment
              in: query
              description: The down payment. Must be at least 5%
                of first $500k plus 10% of any amount above $500k (e.g.
                $50k on a $750k principal).
              required: true
              type: number
            - name: payment_schedule
              in: query
              description: The frequency of payments.
              enum:
                - monthly
                - biweekly
                - weekly
              required: true
              type: string
            - name: amortization_period
              in: query
              description: The mortgage amortization period (min 5 years,
                max 25 years).
              required: true
              type: integer
        responses:
            200:
                description: Success
                schema:
                    required:
                        - recurring_payment
                    properties:
                        recurring_payment:
                            type: string
                            description: The payment amount per payment
                                schedule.
                            required: true
            400:
                description: Invalid input
            500:
                description: Unexpected error
        """
        try:
            asking_price = float(request.args['asking_price'])
            down_payment = float(request.args['down_payment'])
            payment_schedule = str(request.args['payment_schedule'])
            amortization_period = int(request.args['amortization_period'])
        except (ValueError, KeyError):
            raise ValidationError()

        self._validate_amortization_period(amortization_period)
        self._validate_payment_schedule(payment_schedule)

        amount = self.calculator.get_recurring_payment(
            asking_price, down_payment, payment_schedule, amortization_period)

        response = self.server.response_class(
            response=json.dumps({'recurring_payment': '{:.2f}'.format(amount)}),
            status=200,
            mimetype='application/json'
        )

        return response

    def get_maximum_mortgage(self):
        """
        Get the maximum mortgage amount.
        ---
        tags:
            - calculation
        parameters:
            - name: payment_amount
              in: query
              description: The payment amount per payment schedule.
              required: true
              type: number
            - name: payment_schedule
              in: query
              description: The frequency of payments.
              enum:
                - monthly
                - biweekly
                - weekly
              required: true
              type: string
            - name: amortization_period
              in: query
              description: The mortgage amortization period (min 5 years,
                max 25 years).
              required: true
              type: integer
            - name: down_payment
              in: query
              description: The down payment. Must be at least 5%
                of first $500k plus 10% of any amount above $500k (e.g.
                $50k on a $750k principal).
              required: false
              type: number
        responses:
            200:
                description: Success
                schema:
                    required:
                        - maximum_mortgage
                    properties:
                        maximum_mortgage:
                            type: string
                            description: The maximum mortgage that can
                                be taken out.
                            required: true
            400:
                description: Invalid input
            500:
                description: Unexpected error
        """
        try:
            recurring_payment_amount = float(request.args['payment_amount'])
            payment_schedule = str(request.args['payment_schedule'])
            amortization_period = int(request.args['amortization_period'])
            down_payment = float(request.args.get('down_payment', default=0.0))
        except (ValueError, KeyError):
            raise ValidationError()

        self._validate_amortization_period(amortization_period)
        self._validate_payment_schedule(payment_schedule)

        amount = self.calculator.get_maximum_mortgage(
            recurring_payment_amount, payment_schedule, amortization_period,
            down_payment=down_payment)

        response = self.server.response_class(
            response=json.dumps({'maximum_mortgage': '{:.2f}'.format(amount)}),
            status=200,
            mimetype='application/json'
        )

        return response

    def set_interest_rate(self):
        """
        Change the interest rate used by the application.
        ---
        tags:
            - configuration
        parameters:
        - name: body
          in: body
          description: The new interest rate.
          required: true
          schema:
            type: object
            required:
                - new
            properties:
                new:
                    type: number
                    description: The new interest rate.
                    required: true
        responses:
            200:
                description: Success
                schema:
                    required:
                        - old
                        - new
                    properties:
                        old:
                            type: number
                            description: The old interest rate.
                            required: true
                        new:
                            type: number
                            description: The new interest rate.
                            required: true
            400:
                description: Invalid input
            500:
                description: Unexpected error
        """
        try:
            interest_rate = float(request.get_json()['new'])
        except (ValueError, KeyError):
            raise ValidationError()

        old_interest_rate = self.calculator.set_interest_rate(interest_rate)

        response = {
            'old': old_interest_rate,
            'new': interest_rate
        }

        return self.server.response_class(
            response=json.dumps(response),
            status=200,
            mimetype='application/json'
        )

    @staticmethod
    def _validate_amortization_period(amortization_period):
        if (amortization_period < ALLOWED_AMORTIZATION_RANGE_YEARS[0] or
                amortization_period > ALLOWED_AMORTIZATION_RANGE_YEARS[1]):
            error = ('amortization_period must be between %s and %s inclusive'
                     % (ALLOWED_AMORTIZATION_RANGE_YEARS[0],
                        ALLOWED_AMORTIZATION_RANGE_YEARS[1]))

            raise ValidationError(error)

    @staticmethod
    def _validate_payment_schedule(payment_schedule):
        payment_schedule_types = PAYMENT_SCHEDULE_PERIODS.keys()
        if payment_schedule not in payment_schedule_types:
            error = ('payment_schedule must be one of %s'
                     % payment_schedule_types)

            raise ValidationError(error)


if __name__ == "__main__":
    app = CalculatorApp()
    app.run()

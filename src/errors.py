class ApplicationError(Exception):
    pass


class ValidationError(ApplicationError):
    pass


class DownPaymentError(ValidationError):
    pass


class InterestRateError(ValidationError):
    pass

from config import (PAYMENT_SCHEDULE_PERIODS, DEFAULT_INTEREST,
                    DOWN_PAYMENT_INSURANCE_MAP,
                    MAX_MORTGAGE_INSURANCE_THRESHOLD)
from errors import DownPaymentError, InterestRateError


class Calculator(object):
    def __init__(self, interest_rate=DEFAULT_INTEREST):
        self.interest_rate = interest_rate

    def get_recurring_payment(self, principal, down_payment,
                              payment_schedule, amortization_period):
        self._validate_down_payment_amount(principal, down_payment)

        payment_schedule_periods = PAYMENT_SCHEDULE_PERIODS[payment_schedule]
        num_payments = amortization_period * payment_schedule_periods

        insurance_amount = self._get_insurance_amount(principal, down_payment)

        effective_principal = principal - down_payment + insurance_amount
        recurring_payment = self._calculate_recurring_payment(
            effective_principal,
            payment_schedule_periods,
            num_payments)

        return recurring_payment

    def get_maximum_mortgage(self, recurring_payment_amount, payment_schedule,
                             amortization_period, down_payment=0.0):
        payment_schedule_periods = PAYMENT_SCHEDULE_PERIODS[payment_schedule]

        c = self.interest_rate / payment_schedule_periods
        n = amortization_period * payment_schedule_periods

        numerator = recurring_payment_amount * (((1.0 + c)**n) - 1.0)
        denominator = c * ((1.0 + c)**n)

        principle = (numerator / denominator)

        return principle + down_payment

    def set_interest_rate(self, interest_rate):
        if interest_rate <= 0:
            raise InterestRateError('Interest rate must be greater than 0')

        old_interest_rate = self.interest_rate
        self.interest_rate = float(interest_rate)

        return old_interest_rate

    def _calculate_recurring_payment(self, principal,
                                     payment_schedule_periods, num_payments):
        c = self.interest_rate / payment_schedule_periods
        n = num_payments

        numerator = principal * (c * (1.0 + c)**n)
        denominator = ((1.0 + c)**n) - 1.0

        return numerator / denominator

    @staticmethod
    def _validate_down_payment_amount(principal, down_payment):
        first_tier_base = principal if principal < 500000 else 500000
        second_tier_base = 0 if principal <= 500000 else principal - 500000

        min_down_payment = (first_tier_base * 0.05) + (second_tier_base * 0.1)
        if down_payment < min_down_payment:
            raise DownPaymentError(
                'Down payment must be at least {:.2f}'.format(
                    min_down_payment))

    @staticmethod
    def _get_insurance_amount(principal, down_payment):
        if principal > MAX_MORTGAGE_INSURANCE_THRESHOLD:
            return 0

        down_payment_percentage = down_payment / principal
        for ((min_down_percent, max_down_percent), insurance_percentage) \
                in DOWN_PAYMENT_INSURANCE_MAP.iteritems():
            if min_down_percent <= down_payment_percentage < max_down_percent:
                return principal * insurance_percentage

        return 0
